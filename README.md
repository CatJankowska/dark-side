### "Are you afraid of the dark?"

This is a simple application to verify the brightness of images. The input files are located in the folder Photos/in 

````
To start the application:
1. Launch the Intellij IDEA
2. Click Run
3. The output files will be in the folder Photos/out
````

After launching the application, the program goes through the supplied input directory, reads and analyzes all jpg and png files. Then it saves them to the output folder with attached metadata.

The output directory contains files that have been analyzed and contain two additional information.
The value of "bright" or "dark",
depending on the second value - the result of the algorithm and its result between 0 and 100, where 0 is extremely bright and 100 extremely dark.
 
The image brightness threshold is defined as INT_POINT_VALUE
I'ts value directly affects result in Photos/out


