package ScalaPackage

import java.io.{File, IOException}
import scala.language.postfixOps
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

object HelloWorld {
  val INT_POINT_VALUE = 80 // Score value

  val okFileExtensions = List("jpg", "png")
  val files = getListOfFiles(new File("Photos/in"), okFileExtensions) // lists the list of jpg and png files

  def getListOfFiles(dir: File, extensions: List[String]): List[File] = {
    dir.listFiles.filter(_.isFile).toList.filter { file =>
      extensions.exists(file.getName.endsWith(_))
    }
  }

  // gets the file name and extension values
  def getFileNameAndExtension(fileName: String, option: String): String = {
    if (option eq "1") fileName.dropRight(fileName.length - fileName.lastIndexOf("."))
    else fileName.dropWhile(_ != '.')
  }

  // gets image values: height, width and RGB
  // image analysis, assigning brightness points
  // getRGB  tttt tttt rrrr rrrr gggg gggg bbbb bbbb

  def photoCheckAndMove(img: BufferedImage, fileFullName: String): Unit = {
    var color = 0
    var y = 0
    var red = 0
    var green = 0
    var blue = 0
    var luminance = 0.00
    var luminanceSum = 0.00

    while ( {
      y < img.getHeight
    }) {
      var x = 0
      while ( {
        x < img.getWidth
      }) {
        color = img.getRGB(x, y)
        red = (color & 0xff0000) / 65536
        green = (color & 0xff00) / 256
        blue = (color & 0xff)
        luminance = (red * 0.2126f + green * 0.7152f + blue * 0.0722f) / 255
        luminanceSum = luminanceSum + luminance

        {
          x += 1;
          x - 1
        }
      }
      {
        y += 1;
        y - 1
      }
    }

    //calculating the final luminance value, the luminance value for each pixel
    // multiplied by the size and width, and then divided by the number of pixels

    val luminanceFinal = (luminanceSum / (img.getHeight * img.getWidth));
    val luminanceToPoints = ((luminanceFinal * 100).round - 100).abs
    val fileName = getFileNameAndExtension(fileFullName, "1");
    val extension = getFileNameAndExtension(fileFullName, "2");

    println("LUMINANCE = " + luminanceFinal + " POINTS: " + luminanceToPoints)

    if (luminanceToPoints >= INT_POINT_VALUE) {
      println(fileFullName + "  ----> It's too dark!")
      ImageIO.write(img, "jpg", new File(("Photos/out/" + fileName + "_dark_" + luminanceToPoints + extension))) //Move dark photos
    }
    else {
      println(fileFullName + "  ----> It's bright enough")
      ImageIO.write(img, "jpg", new File(("Photos/out/" + fileName + "_bright_" + luminanceToPoints + extension))) //Move bright photos
    }
  }

  def test() {
    for (file <- files) {
      val image = ImageIO.read(new File(file.toString))
      println("")
      photoCheckAndMove(image, file.getName)
    }
  }

  def main(args: Array[String]): Unit = {
    test()
  }
}